# TravelApp

TravelApp is a university class project

# Technology
TypeScript - is an open-source language that builds on JavaScript, one of the world's most used tools, by adding static type definitions.

Angular - is an application design framework and development platform for creating efficient and sophisticated single-page apps.

Bootstrap - Bootstrap is a free and open-source CSS framework directed at responsive, mobile-first front-end web development.

## Installation
```bash
npm install -g @angular/cli
```

## How to run the application

```bash
$ git clone https://michalkubis97@bitbucket.org/michalkubis97/travelfront.git
$ ng serve
```
## Simple code example
```
import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
```
## Description
The main goal of this project is to create an application that allows users to book their tickets for events or hotel rooms on the same website.

## Authors
Marcin Flądro & Michał Kubis
