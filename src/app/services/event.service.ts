import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  private url = 'http://localhost:8080/event';

  constructor(private http:HttpClient) { }

  getUpcomingEvents(token:string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    })

    return this.http.get(this.url+'/get-upcoming-events', {headers:headers});
  }

  getUpcomingEventsByCity(city:string, token:string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    })

    return this.http.get(this.url+'/get-upcoming-events-by-city/'+city, {headers:headers});
  }

  getUpcomingEventsByType(type:string, token:string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    })

    return this.http.get(this.url+'/get-upcoming-events-by-type/'+type, {headers:headers});
  }

  getEventById(id, token:string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    })

    return this.http.get(this.url+'/get-event-by-id/'+id, {headers:headers});
  }
}
