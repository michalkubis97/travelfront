import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = 'http://localhost:8080/user';
  
  constructor(private http:HttpClient) { }

  register(data?:any){
    return this.http.post(this.url+'/register', data);
  }

  getLoggedUser(username:string, token:string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    })

    return this.http.get(this.url+'/get-logged-user/'+username, {headers:headers});
  }


}
