import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class HotelService {
  private url = 'http://localhost:8080/hotel';
  
  constructor(private http: HttpClient, private cookies:CookieService) { }

  getAllHotels(token:string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    })

    return this.http.get(this.url+'/get-all-hotels', {headers:headers});
  }

  getHotelsByCity(city:string, token:string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    })

    return this.http.get(this.url+'/get-hotels-by-city/'+city, {headers:headers});
  }

  getHotelsByStars(stars:string, token:string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    })

    return this.http.get(this.url+'/get-hotels-by-stars/'+stars, {headers:headers});
  }
  
  getHotelById(id, token:string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    })

    return this.http.get(this.url+'/get-hotel-by-id/'+id, {headers:headers});
  }

  getRoomsByHotelId(id, token:string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    })

    return this.http.get(this.url+'/get-rooms-by-hotel-id/'+id, {headers:headers});
  }
}
