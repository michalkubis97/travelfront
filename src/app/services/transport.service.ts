import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TransportService {
  private url = 'http://localhost:8080/transport';

  constructor(private http:HttpClient) { }

  getAllTransports(token:string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    })

    return this.http.get(this.url+'/get-all-transports', {headers:headers});
  }

  getTransportsByDestination(from:string, to:string, token:string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    })

    return this.http.get(this.url+'/get-transports-by-destination/'+from+'/'+to, {headers:headers});
  }

  getTransportById(id, token:string){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    })

    return this.http.get(this.url+'/get-transport-by-id/'+id, {headers:headers});
  }

}