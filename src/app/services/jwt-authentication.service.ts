import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class JwtAuthenticationService implements CanActivate {
  private url = 'http://localhost:8080/authenticate';

  constructor(private http:HttpClient, private cookies:CookieService, private router:Router) { }

  authenticate(username:string, password:string){
    return this.http.post(this.url, new LoginRequest(username,password));
  }

  canActivate() : boolean{
    if(this.cookies.check('token')==false){
      this.router.navigate(['/login'])
      return false;
    }
    return true;
  }
 
}

class LoginRequest{
  username:string;
  password:string;

  constructor(username:string, password:string){
    this.username = username;
    this.password = password;
  }
}