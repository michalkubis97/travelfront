class EventTicket{
    eventId:number;
    orderId:number;
    quantity:number;
    totalCost:number

    constructor(eventId:number, quantity:number, totalCost:number){
        this.eventId = eventId;
        this.quantity = quantity;
        this.totalCost = totalCost;
        this.orderId = -1;
    }
}