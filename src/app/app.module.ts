import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/user-components/login/login.component';
import { RegisterComponent } from './components/user-components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HotelListComponent } from './components/hotel-components/hotel-list/hotel-list.component';
import { HotelService } from './services/hotel.service';
import { HttpClientModule } from '@angular/common/http';
import { HotelItemComponent } from './components/hotel-components/hotel-item/hotel-item.component';
import { HotelsComponent } from './components/hotel-components/hotels/hotels.component';
import { FilteredHotelsComponent } from './components/hotel-components/filtered-hotels/filtered-hotels.component';
import { HotelDetailsComponent } from './components/hotel-components/hotel-details/hotel-details.component';
import { RoomComponent } from './components/hotel-components/room/room.component';
import { FilteredHotelListComponent } from './components/hotel-components/filtered-hotel-list/filtered-hotel-list.component';
import { EventsComponent } from './components/event-components/events/events.component';
import { RegisterSuccessComponent } from './components/user-components/register-success/register-success.component';
import { EventListComponent } from './components/event-components/event-list/event-list.component';
import { EventItemComponent } from './components/event-components/event-item/event-item.component';
import { FilteredEventsComponent } from './components/event-components/filtered-events/filtered-events.component';
import { FilteredEventListComponent } from './components/event-components/filtered-events-list/filtered-event-list.component';
import { EventDetailsComponent } from './components/event-components/event-details/event-details.component';
import { TransportsComponent } from './components/transport-components/transports/transports.component';
import { TransportListComponent } from './components/transport-components/transport-list/transport-list.component';
import { TransportItemComponent } from './components/transport-components/transport-item/transport-item.component';
import { TransportDetailsComponent } from './components/transport-components/transport-details/transport-details.component';
import { FilteredTransportsComponent } from './components/transport-components/filtered-transports/filtered-transports.component';
import { FilteredTransportListComponent } from './components/transport-components/filtered-transport-list/filtered-transport-list.component';
import { EventService } from './services/event.service';
import { TransportService } from './services/transport.service';
import { UserService } from './services/user.service';
import { JwtAuthenticationService } from './services/jwt-authentication.service';
import { CookieService } from 'ngx-cookie-service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    NavbarComponent,
    HotelListComponent,
    HotelItemComponent,
    HotelsComponent,
    FilteredHotelsComponent,
    HotelDetailsComponent,
    RoomComponent,
    FilteredHotelListComponent,
    EventsComponent,
    RegisterSuccessComponent,
    EventListComponent,
    EventItemComponent,
    FilteredEventsComponent,
    FilteredEventListComponent,
    EventDetailsComponent,
    TransportsComponent,
    TransportListComponent,
    TransportItemComponent,
    TransportDetailsComponent,
    FilteredTransportsComponent,
    FilteredTransportListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [HotelService,EventService,TransportService,UserService,JwtAuthenticationService, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
