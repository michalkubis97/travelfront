import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FilteredHotelsComponent } from './components/hotel-components/filtered-hotels/filtered-hotels.component';
import { HomeComponent } from './components/home/home.component';
import { HotelDetailsComponent } from './components/hotel-components/hotel-details/hotel-details.component';
import { HotelsComponent } from './components/hotel-components/hotels/hotels.component';
import { LoginComponent } from './components/user-components/login/login.component';
import { RegisterComponent } from './components/user-components/register/register.component';
import { RegisterSuccessComponent } from './components/user-components/register-success/register-success.component';
import { EventsComponent } from './components/event-components/events/events.component';
import { EventDetailsComponent } from './components/event-components/event-details/event-details.component';
import { FilteredEventsComponent } from './components/event-components/filtered-events/filtered-events.component';
import { TransportsComponent } from './components/transport-components/transports/transports.component';
import { TransportDetailsComponent } from './components/transport-components/transport-details/transport-details.component';
import { FilteredTransportsComponent } from './components/transport-components/filtered-transports/filtered-transports.component';
import { JwtAuthenticationService } from './services/jwt-authentication.service';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'register/success',
    component: RegisterSuccessComponent
  },
  {
    path: 'hotel',
    component: HotelsComponent,
    canActivate: [JwtAuthenticationService]
  },
  {
    path: 'hotel/details/:id',
    component: HotelDetailsComponent,
    canActivate: [JwtAuthenticationService]
  },
  {
    path: 'hotel/filter/:by/:value',
    component: FilteredHotelsComponent,
    canActivate: [JwtAuthenticationService]
  },
  {
    path: 'event',
    component: EventsComponent,
    canActivate: [JwtAuthenticationService]
  },
  {
    path: 'event/details/:id',
    component: EventDetailsComponent,
    canActivate: [JwtAuthenticationService]
  },
  {
    path: 'event/filter/:by/:value',
    component: FilteredEventsComponent,
    canActivate: [JwtAuthenticationService]
  },
  {
    path: 'transport',
    component: TransportsComponent,
    canActivate: [JwtAuthenticationService]
  },
  {
    path: 'transport/details/:id',
    component: TransportDetailsComponent,
    canActivate: [JwtAuthenticationService]
  },
  {
    path: 'transport/filter/destination/:from/:to',
    component:FilteredTransportsComponent,
    canActivate: [JwtAuthenticationService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation:'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
