import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'event-item',
  templateUrl: './event-item.component.html',
  styleUrls: ['./event-item.component.css']
})
export class EventItemComponent implements OnInit {
  @Input() id:number;
  @Input() type:string;
  @Input() city:string;
  @Input() address:string;
  @Input() name:string;
  @Input() description:string;
  @Input() start:Date;
  @Input() end:Date;
  @Input() ticketCost:number;
  @Input() image:string;

  constructor() { }

  ngOnInit(): void {
  }

}
