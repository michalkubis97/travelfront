import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilteredEventListComponent } from './filtered-event-list.component';

describe('FilteredEventsListComponent', () => {
  let component: FilteredEventListComponent;
  let fixture: ComponentFixture<FilteredEventListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilteredEventListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilteredEventListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
