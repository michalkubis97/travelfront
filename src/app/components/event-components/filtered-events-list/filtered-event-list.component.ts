import { Component, Input, OnChanges } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'filtered-event-list',
  templateUrl: './filtered-event-list.component.html',
  styleUrls: ['./filtered-event-list.component.css']
})
export class FilteredEventListComponent implements OnChanges {
  @Input() by:string;
  @Input() value:string;
  items$:any;
  
  constructor(private service:EventService, private cookies:CookieService) { }

  ngOnChanges(): void {
    switch(this.by){
      case 'city':{
        this.service.getUpcomingEventsByCity(this.value, this.cookies.get('token')).subscribe((response)=>{
          this.items$ = response;
        })
        break;
      }
      case 'type':{
        this.service.getUpcomingEventsByType(this.value, this.cookies.get('token')).subscribe((response)=>{
          this.items$ = response;
        })
        break;
      }
    }
  }

}
