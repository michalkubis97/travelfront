import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  by: string;
  valueCity = new FormControl('', Validators.required)
  valueType: string = "MUSIC";

  constructor(private router:Router) { }

  ngOnInit(): void {
    this.by = 'city';
  }

  navigate(){
    let value = '';
    if(this.by === 'city'){
      value = this.valueCity.value
    }else if (this.by === 'type'){
      value = this.valueType
    }
    this.router.navigate(['event/filter', this.by, value])
  }

}
