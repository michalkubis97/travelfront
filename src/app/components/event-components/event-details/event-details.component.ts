import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.css']
})
export class EventDetailsComponent implements OnInit {
  id: number;
  type: string;
  city: string;
  address: string;
  name: string;
  description: string;
  start: Date;
  end: Date;
  ticketCost: number;
  image: string;

  constructor(private route: ActivatedRoute, private service: EventService, private cookies: CookieService) { }

  ngOnInit(): void {
    let id: string;
    this.route.paramMap.subscribe((params) => {
      id = params.get('id');
    });

    this.service.getEventById(id,this.cookies.get('token')).subscribe((response)=>{
      this.id = response['id']
      this.type = response['type']
      this.city = response['city']
      this.address = response['address']
      this.name = response['name']
      this.description = response['description']
      this.start = response['start']
      this.end = response['end']
      this.ticketCost = response['ticketCost']
      this.image = response['imageUrl']
    });
  }

}
