import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {
  items$:any;
  constructor(private service:EventService, private cookies:CookieService) { }

  ngOnInit(): void {
    this.service.getUpcomingEvents(this.cookies.get('token')).subscribe((response)=>{
      this.items$ = response;
    })
  }

}
