import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-filtered-events',
  templateUrl: './filtered-events.component.html',
  styleUrls: ['./filtered-events.component.css']
})
export class FilteredEventsComponent implements OnInit {
  by:string;
  valueCity: FormControl;
  valueType:string;
  
  currentBy:string;
  currentValue:string;

  constructor(private route:ActivatedRoute, private router:Router) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.currentBy = params.get('by');
      this.currentValue = params.get('value');
      
      this.by=params.get('by');
      if(this.by=='city'){
        this.valueCity = new FormControl(params.get('value'), Validators.required);
        this.valueType = 'MUSIC';
      }else if(this.by=='type'){
        this.valueType = params.get('value');
        this.valueCity = new FormControl('', Validators.required);
      }
    })
  }

  navigate(){
    let value = '';
    if(this.by=='city'){
      value = this.valueCity.value
    }else if(this.by=='type'){
      value = this.valueType
    }
    this.router.navigate(['event/filter', this.by, value])
  }
}
