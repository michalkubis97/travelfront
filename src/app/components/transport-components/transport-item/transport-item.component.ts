import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'transport-item',
  templateUrl: './transport-item.component.html',
  styleUrls: ['./transport-item.component.css']
})
export class TransportItemComponent implements OnInit {
  @Input() id:number;
  @Input() type:string;
  @Input() transportClass:string;
  @Input() contractor:string;
  @Input() from:string;
  @Input() to:string;
  @Input() numberOfSeats:number;
  @Input() ticketCost:number;
  @Input() websiteUrl:string;
  @Input() logoUrl:string;

  constructor() { }

  ngOnInit(): void {
  }

}
