import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'filtered-transports',
  templateUrl: './filtered-transports.component.html',
  styleUrls: ['./filtered-transports.component.css']
})
export class FilteredTransportsComponent implements OnInit {
  from:string;
  to:string;
  constructor(private formBuilder:FormBuilder ,private router:Router, private route:ActivatedRoute) { }

  destinationForm;

  ngOnInit(): void {
    this.route.paramMap.subscribe((params)=>{
      this.from = params.get("from")
      this.to = params.get("to")

      this.destinationForm = this.formBuilder.group({
        from: [params.get("from"), Validators.required],
        to: [params.get("to"), Validators.required]
      })
    })
    
    
  }

  navigate(){
    this.router.navigate(['transport/filter/destination', this.destinationForm.value.from, this.destinationForm.value.to])
  }
}
