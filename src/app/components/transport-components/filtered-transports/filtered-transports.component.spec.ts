import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilteredTransportsComponent } from './filtered-transports.component';

describe('FilteredTransportsComponent', () => {
  let component: FilteredTransportsComponent;
  let fixture: ComponentFixture<FilteredTransportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilteredTransportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilteredTransportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
