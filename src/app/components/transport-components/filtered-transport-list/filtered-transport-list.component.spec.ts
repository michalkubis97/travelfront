import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilteredTransportListComponent } from './filtered-transport-list.component';

describe('FilteredTransportListComponent', () => {
  let component: FilteredTransportListComponent;
  let fixture: ComponentFixture<FilteredTransportListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilteredTransportListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilteredTransportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
