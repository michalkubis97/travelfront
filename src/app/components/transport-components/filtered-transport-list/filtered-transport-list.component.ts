import { Component, Input, OnChanges } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { TransportService } from 'src/app/services/transport.service';

@Component({
  selector: 'filtered-transport-list',
  templateUrl: './filtered-transport-list.component.html',
  styleUrls: ['./filtered-transport-list.component.css']
})
export class FilteredTransportListComponent implements OnChanges {
  @Input() from:string;
  @Input() to:string;
  items$:any;

  constructor(private service:TransportService, private cookies:CookieService) { }

  ngOnChanges(): void {
    this.service.getTransportsByDestination(this.from,this.to, this.cookies.get('token')).subscribe((response)=>{
      this.items$ = response;
    })
  }

}
