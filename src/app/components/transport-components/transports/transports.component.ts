import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { from } from 'rxjs';

@Component({
  selector: 'transports',
  templateUrl: './transports.component.html',
  styleUrls: ['./transports.component.css']
})
export class TransportsComponent implements OnInit {
  
  constructor(private formBuilder:FormBuilder, private router:Router) { }

  destinationForm = this.formBuilder.group({
    from: ['', Validators.required],
    to: ['', Validators.required]
  })
  
  ngOnInit(): void {
  }

  navigate(){
    this.router.navigate(['transport/filter/destination', this.destinationForm.value.from, this.destinationForm.value.to])
  }

}
