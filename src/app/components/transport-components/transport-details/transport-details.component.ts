import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { TransportService } from 'src/app/services/transport.service';

@Component({
  selector: 'transport-details',
  templateUrl: './transport-details.component.html',
  styleUrls: ['./transport-details.component.css']
})
export class TransportDetailsComponent implements OnInit {
  id:number;
  type:string;
  transportClass:string;
  contractor:string;
  from:string;
  to:string;
  numberOfSeats:number;
  ticketCost:number;
  websiteUrl:string;
  logoUrl:string;
  
  constructor(private service:TransportService, private route:ActivatedRoute, private cookies:CookieService) { }

  ngOnInit(): void {
    let id:string;
    this.route.paramMap.subscribe((params) => {
      id = params.get('id')
    });
    
    this.service.getTransportById(id, this.cookies.get('token')).subscribe((response)=>{
      this.id=response['id'];
      this.type=response['type'];
      this.transportClass=response['transportClass']
      this.contractor=response['contractor']
      this.from=response['fromDestination']
      this.to=response['toDestination']
      this.numberOfSeats=response['numberOfSeats']
      this.ticketCost=response['ticketCost']
      this.websiteUrl=response['websiteUrl']
      this.logoUrl=response['logoUrl']
    })
  }

}
