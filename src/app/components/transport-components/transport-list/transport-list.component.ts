import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { TransportService } from 'src/app/services/transport.service';

@Component({
  selector: 'transport-list',
  templateUrl: './transport-list.component.html',
  styleUrls: ['./transport-list.component.css']
})
export class TransportListComponent implements OnInit {
  items$: any;

  constructor(private service:TransportService, private cookies:CookieService) { }

  ngOnInit(): void {
    this.service.getAllTransports(this.cookies.get('token')).subscribe((response)=>{
      this.items$ = response;
    })
  }

}
