import { Component, OnInit } from '@angular/core';
import { FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit {
  by: string;
  valueCity = new FormControl('', Validators.required);
  valueStars:string = "ONE";

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.by = 'city';
  }

  navigate(){
    let value = '';
    if (this.by === 'city'){
      value = this.valueCity.value;
    }else if (this.by === 'stars'){
      value = this.valueStars;
    }
    this.router.navigate(['hotel/filter', this.by, value]);
  }

}
