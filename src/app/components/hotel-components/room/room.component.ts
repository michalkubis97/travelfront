import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {
  @Input() roomId:number;
  @Input() roomNumber:number;
  @Input() hotelId:number;
  @Input() standard:string;
  @Input() numberOfBeds:number;
  @Input() dailyCost:number;
  @Input() available:boolean;
  @Input() image:string;

  constructor() { }

  ngOnInit(): void {
  }

}
