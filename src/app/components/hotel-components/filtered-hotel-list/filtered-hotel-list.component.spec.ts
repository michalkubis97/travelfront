import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilteredHotelListComponent } from './filtered-hotel-list.component';

describe('FilteredHotelListComponent', () => {
  let component: FilteredHotelListComponent;
  let fixture: ComponentFixture<FilteredHotelListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilteredHotelListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilteredHotelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
