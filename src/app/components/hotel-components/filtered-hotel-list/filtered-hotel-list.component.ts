import { Component, Input, OnChanges} from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { HotelService } from 'src/app/services/hotel.service';

@Component({
  selector: 'filtered-hotel-list',
  templateUrl: './filtered-hotel-list.component.html',
  styleUrls: ['./filtered-hotel-list.component.css']
})
export class FilteredHotelListComponent implements OnChanges {
  @Input() by:string;
  @Input() value:string;
  items$:any;

  constructor(private service:HotelService, private cookies:CookieService) { }

  ngOnChanges(): void {
    switch(this.by){
      case 'city':{
        this.service.getHotelsByCity(this.value, this.cookies.get('token')).subscribe((response)=>{
          this.items$ = response;
        })
        break;
      }
      case 'stars':{
        this.service.getHotelsByStars(this.value, this.cookies.get('token')).subscribe((response)=>{
          this.items$ = response;
        })
        break;
      }
    }
  }

}
