import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'hotel-item',
  templateUrl: './hotel-item.component.html',
  styleUrls: ['./hotel-item.component.css']
})
export class HotelItemComponent implements OnInit {
  

  @Input() id:number;
  @Input() city:string;
  @Input() name:string;
  @Input() address:string;
  @Input() stars:string;
  @Input() image:string;

  
  constructor() { }

  ngOnInit(): void {
  }

}
