import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { HotelService } from 'src/app/services/hotel.service';

@Component({
  selector: 'hotel-details',
  templateUrl: './hotel-details.component.html',
  styleUrls: ['./hotel-details.component.css']
})
export class HotelDetailsComponent implements OnInit {
  id: number;
  city: string;
  name: string;
  address: string;
  stars: string;
  image: string;
  rooms$: any;

  constructor(private service: HotelService, private route: ActivatedRoute, private cookies: CookieService) { }

  ngOnInit(): void {
    let id:string;
    this.route.paramMap.subscribe((params) => {
      id = params.get('id')
    });

    this.service.getHotelById(id, this.cookies.get('token')).subscribe((response) => {
      this.id = response['id']
      this.city = response['city']
      this.name = response['name']
      this.address = response['address']
      this.stars = response['stars']
      this.image = response['imageUrl']
    })

    this.service.getRoomsByHotelId(id, this.cookies.get('token')).subscribe((response) => {
      this.rooms$ = response;
    });

  }

}
