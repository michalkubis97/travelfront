import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { HotelService } from 'src/app/services/hotel.service';

@Component({
  selector: 'hotel-list',
  templateUrl: './hotel-list.component.html',
  styleUrls: ['./hotel-list.component.css']
})
export class HotelListComponent implements OnInit {
  public items$: any;
  constructor(private service:HotelService, private cookies:CookieService) { }

  ngOnInit(): void {
    this.service.getAllHotels(this.cookies.get('token')).subscribe((response)=>{
      this.items$ = response;
    })
  }

}
