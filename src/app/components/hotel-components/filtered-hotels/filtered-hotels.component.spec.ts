import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilteredHotelsComponent } from './filtered-hotels.component';

describe('FilteredHotelsComponent', () => {
  let component: FilteredHotelsComponent;
  let fixture: ComponentFixture<FilteredHotelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilteredHotelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilteredHotelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
