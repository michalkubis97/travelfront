import { Component, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'filtered-hotels',
  templateUrl: './filtered-hotels.component.html',
  styleUrls: ['./filtered-hotels.component.css'],
})
export class FilteredHotelsComponent implements OnInit {
  by:string;
  valueCity: FormControl;
  valueStars:string;
  
  currentBy:string;
  currentValue:string;

  constructor(private route:ActivatedRoute, private router:Router) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.currentBy = params.get('by');
      this.currentValue = params.get('value');
      
      this.by=params.get('by');
      if(this.by=='city'){
        this.valueCity = new FormControl(params.get('value'), Validators.required);
        this.valueStars = 'ONE';
      }else if(this.by=='stars'){
        this.valueStars = params.get('value');
        this.valueCity = new FormControl('', Validators.required);
      }
    });
  }

  navigate(){
    let value = '';
    if(this.by=='city'){
      value = this.valueCity.value
    }else if(this.by=='stars'){
      value = this.valueStars
    }
    this.router.navigate(['hotel/filter', this.by, value])
  }
}
