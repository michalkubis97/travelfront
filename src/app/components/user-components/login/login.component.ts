import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { JwtAuthenticationService } from 'src/app/services/jwt-authentication.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username = ''
  password = ''
  loginFailed = false;

  constructor(
    private cookies:CookieService,
    private authentication:JwtAuthenticationService,
    private router:Router
    ) { }

  ngOnInit(): void {
    
  }

  login(){
    this.authentication.authenticate(this.username,this.password).subscribe((response)=>{
      var expires = new Date(Date.now());
      expires.setHours(expires.getHours()+5);
      var token = 'Bearer ' + response['token'];
      var userId = response['userId'];

      this.setCookies(token, userId, expires);

      this.router.navigate(['/']);
      },
      (err)=>{this.loginFailed = true;});
  }

  setCookies(token:string, userId:string, expires:Date){
    this.cookies.set('token', token);
    this.cookies.set('userId', userId);
  }

}
