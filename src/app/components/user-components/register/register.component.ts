import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  constructor(private formBuilder: FormBuilder, private service:UserService, private router:Router) { }
 
  registerForm = this.formBuilder.group({
  username: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(13)]],
  password: ['', [Validators.required, Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{5,13}$')]],
  email: ['', [Validators.required, Validators.email]],
  firstName: ['', [Validators.required, Validators.pattern('^[A-ZŻŹĆĄŚĘŁÓŃ]+[a-zżźćńółęąś]*')]],
  lastName: ['',[ Validators.required,  Validators.pattern('^[A-ZŻŹĆĄŚĘŁÓŃ]+[a-zżźćńółęąś]*')]],
  city: ['', [Validators.required,  Validators.pattern('^[A-ZŻŹĆĄŚĘŁÓŃ]+[a-zżźćńółęąś]*')]],
  postcode: ['', [Validators.required, Validators.pattern('^[0-9][0-9]-[0-9][0-9][0-9]$')]],
  address: ['', Validators.required],
  phone: ['', [Validators.required, Validators.pattern('^([0-9]{3})([ -]?)([0-9]{3})([ -]?)([0-9]{3})$')]],
  });
  

  ngOnInit(): void {
  }

  register(){
    let data = new CreateUser()
    data = this.registerForm.value;
    this.service.register(data).subscribe((response)=>{
      console.log(response);
      this.router.navigate(['register/success']);
    })
  }


}

class CreateUser{
  username: string;
  password: string;
  email: string;
  firstName: string;
  lastName: string;
  city: string;
  postcode: string;
  address: string;
  phone: string;
  }