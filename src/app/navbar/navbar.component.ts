import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { JwtAuthenticationService } from '../services/jwt-authentication.service';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private cookies:CookieService, private router:Router, private service:JwtAuthenticationService) { }

  ngOnInit(): void {
  }

  logout(){
    this.cookies.delete('token');
    this.cookies.delete('userId');
   
    this.router.navigate(['/login']);
  }

}
